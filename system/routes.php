<?php

	class System_routes {
		function __construct($path){
			
/*ISSET PATH*/if(isset($path[0]) && !empty($path[0])){                 
				$ctrl = $path[0];
			
/*ISSET FILE*/	if(file_exists('Controllers/'.ucfirst($ctrl).'.php')){
					include 'Controllers/'.ucfirst($ctrl).'.php';
					$ctrl = ucfirst($ctrl);
		
/*ISSET Class*/		if(class_exists($ctrl,false)){
						$ctrl_ob = new $ctrl;
						
/*ISSET METHOD*/		if(isset($path[1]) && !empty ($path[1])){
							$method = $path[1];
			
/*ISSET METHOD PARAMS*/		if(method_exists($ctrl_ob, $method)){	
								if(isset($path[2]) && !empty($path[2])){
									$params = array_slice($path, 2); 
									call_user_func_array([$ctrl_ob,$method],$params);
								}else{
									$ctrl_ob->$method();
								}					
								
							}else{
								echo "Error  $method method not founds";
							}
							
						}else{
							$ctrl_ob->index();
						}
						
					}else{
						echo "Error $ctrl class not founds";
					}
					
				}else{
					echo "Error $ctrl file not founds";
				}
				
			}else{
				include 'Controllers/Home.php';
				$obj = new Home;
				$obj->index();
			}
		}
	}