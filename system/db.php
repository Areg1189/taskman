<?php

/*************************************************************/

class System_db
{
    public $con;

    function __construct()
    {
        $this->con = new Mysqli('localhost', 'db_name', 'db_name', 'db_name');

        if ($this->con->connect_errno) {
            echo $this->con->connect_error;
            exit;
        }
    }

    public function escape($str)
    {
        $str = htmlspecialchars($str);
        $str = trim($str);
        $str = stripslashes($str);
        $str = htmlentities($str);
        $str = strip_tags($str);
        $str = $this->con->real_escape_string($str);
        return $str;
    }

    /*************************SELECT FUNCTION************************/

    public function select($query, $count = 0)
    {
        $q = $this->con->query($query);
        if ($q->num_rows > 0) {
            if ($count == 1) {
                return $q->fetch_assoc();
            } else {
                $data = [];
                while ($row = $q->fetch_assoc()) {
                    $data[] = $row;
                }
                return $data;
            }
        } else {
            return [];
        }

    }

    /*************************INSERT FUNCTION************************/

    public function insert($table, $this_val)
    {
        $name = [];
        $value = [];
        foreach ($this_val as $key => $values) {
            $name[] = $key;
            $value[] = $this->escape($values);
        }
        $name = implode(',', $name);
        $value = implode("','", $value);
        $insert_query = "insert into $table ($name) values('$value')";
        //var_dump($insert_query);
        //exit;
        if (!$insert_query) {
            echo $this->con->connect_error;
        }
        return $this->con->query($insert_query);

    }

    /*************************UPDATE FUNCTION************************/

    public function update($table, $this_val, $update_where)
    {
        $name = [];
        foreach ($this_val as $key => $values) {
            $name[] = $key . "= '" . $this->escape($values) . "'";
        }
        $name = implode(',', $name);
        $update = "update $table set $name where $update_where";
        if (!$update) {
            echo $this->con->connect_error;
        }
        return $this->con->query($update);
    }

    /*************************DELETE FUNCTION************************/

    public function delate($table, $fild_name, $this_val)
    {
        $del = "delete from $table where $fild_name = '$this_val' ";
        return $this->con->query($del);
    }
}


?>