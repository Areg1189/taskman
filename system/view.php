<?php
	class System_view {
		public function render($file_name,$layout = true){
			if(file_exists('views/'.$file_name.'.php')){
				if($layout){
					include 'views/layout/header.php';
					include 'views/'.$file_name.'.php';
					include 'views/layout/footer.php';
				}else{
					include 'views/'.$file_name.'.php';
				}
			}else{
				echo "invalid file name";
			}
		}
	}