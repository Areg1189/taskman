<section class="container mt-3">
    <div class="row">
        <div class="col-sm-6 mx-auto">
            <form class="bg-light p-3 userForm" action="/taskman/store" method="post">
                <div class="form-group">
                    <label for="username">Имя</label>
                    <input type="text" name="user_name" class="form-control" id="username" placeholder="Имя">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" name="user_email" class="form-control" id="email" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <label for="message" class="d-block">Текст Задачи</label>
                    <textarea name="text" id="message" cols="" rows="5" class="d-block message" placeholder="Текст Задачи"></textarea>
                </div>

                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input"  id="file04">
                        <label class="custom-file-label" for="file04">Выберите картинку</label>

                    </div>

                    <div id="vanilla-demo" class="mt-3">
                    </div>

                </div>
                <div class="d-block mb-3">
                    <span class="err">Формат файла не праавельный. Выберите файл JPG,PNG или GIF</span>
                </div>
                <div class="form-group action">
                    <button type="submit" class="btn btn-primary" name="create" value="create">Сохранить</button>
                    <button type="button" data-toggle="modal" data-target="#live" class="btn btn-info vanilla-result">
                        Предварительный просмотр
                    </button>
                </div>
                <input type="hidden" value="" name="image" />
            </form>
        </div>
    </div>


</section>


<!-- Modal -->
<div class="modal fade" id="live" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered my-own-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Предварительный просмотр</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-5 minImg">
                        <img src="/public/img/default.jpg" alt="" width="100%" class="see">
                    </div>
                    <div class="col-sm-7">
                        <p><span class="font-weight-bold">Имени Пользователя</span> : <span class="userName"></span></p>
                        <p><span class="font-weight-bold">E-mail</span> : <span class="userEmail"></span></p>
                        <p><span class="font-weight-bold">Текст Задачи</span> : <span class="userMessage"></span></p>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>