
<h1 class="text-center">Приложение Задачник</h1>
    <div class="row justify-content-end ">
         <div class="col-3">
            <div class="form-group">
                <label for="sort">Сортировать по :</label>
                <select class="form-control sort" id="sort">
                    <option value=""> -- Выбрать -- </option>
                    <option value="user_name" <?= $this->sort == 'user_name' ? 'selected':''?>>Имя Пользователя</option>
                    <option value="user_email" <?= $this->sort == 'user_email' ? 'selected':''?>>E-mail</option>
                    <option value="status" <?= $this->sort == 'status' ? 'selected':''?>>Статус</option>
                </select>
            </div>
        </div>
    </div>

<div class="result_content">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имени Пользователя</th>
            <th scope="col">E-mail</th>
            <th scope="col">Текст Задачи</th>
            <th scope="col">Статус</th>
            <th scope="col">Картинка</th>
            <th scope="col">Изменить </th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($this->results['rows']) > 0): ?>
            <?php foreach ($this->results['rows'] as $row): ?>
                <tr>
                    <th scope="row"><?= $row['id']?></th>
                    <td><?= $row['user_name']?></td>
                    <td><?= $row['user_email']?></td>
                    <td><?= $row['text']?></td>
                    <td><span class="badge badge-pill badge-<?= $row['status'] == 1 ? 'success' : 'light' ?>"><?= $row['status'] == 1 ? 'выполнен' : 'не выполнен ' ?></span></td>
                    <td><img src="/<?= ($row['images'] != '' ? $row['images'] :'public/img/default.jpg')?>" class="rounded" ></td>
                    <td><button data-item="<?= $row['id']?>" data-toggle="modal" data-target="#edit-modal" class="btn btn-primary edit-bin" type="submit">Изменить</button></td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td><p>Нету Задач </p></td>
            </tr>
        <?php endif; ?>

        </tbody>
    </table>
    <ul class="pagination">
        <?= $this->results['paginationCtrls'] ?>
    </ul>

</div>


<!-- Modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body edit-content">
                <div class="loader"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>