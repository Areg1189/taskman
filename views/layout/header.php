<!DOCTYPE html>
<html lang="en">
<head>
    <title>Taskman</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/css/crop.min.css">
    <link rel="stylesheet" href="/public/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="/">Logo</a>

        <!-- Links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/">Главная </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/taskman/create">Создать Задачу</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <?php if (isset($_SESSION['auth'])): ?>
                <?php if (isset($_SESSION['auth']['admin']) && $_SESSION['auth']['admin'] == true): ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="/admin">Админ <span class="sr-only">(current)</span></a>
                    </li>
                <?php endif; ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/auth/logout">Выйти <span class="sr-only">(current)</span></a>
                </li>
            <?php else: ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/auth/login">Войти <span class="sr-only">(current)</span></a>
                </li>
            <?php endif; ?>
        </ul>
    </nav>

    <?php if (isset($_SESSION['message']) && !empty($_SESSION['message'])): ?>
        <div class="alert alert-<?= $_SESSION['message']['alert'] ?> alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?= $_SESSION['message']['text'] ?>
        </div>
    <?php endif; ?>
