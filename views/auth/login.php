
<div class="row justify-content-center ">
    <div class="col-4  text-center">

        <form class="form-signin" action="/auth/login" method="post">
            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
            <label for="inputLogin" class="sr-only">Логин</label>
            <input type="text" id="inputLogin" class="form-control" name="login" placeholder="Логин" required autofocus="">
            <label for="inputPassword" class="sr-only">Пароль</label>
            <input type="password" id="inputPassword" class="form-control" name="pass" placeholder="Пароль" required>
            <button class="btn btn-lg btn-primary btn-block mt-5 mb-3" name="loginForm" value="login" type="submit">Login</button>
        </form>
    </div>

</div>