$('.sort').change(function () {
    var sort = $(this).val();
    var url = window.location.pathname;
    if (sort) {
        url += '?sort=' + sort;
    }
    window.location.href = url;

});

$('.edit-bin').click(function () {
    var task = $(this).data('item');
    content = $('.edit-content');
    content.html('<div class="loader"></div>');
    $.ajax({
        url: '/admin/edit/' + task,
        method: 'post',
        dataType: 'json',
        success: function (data) {
            var result = '';
            if (data.id) {
                var status = '';
                var status_class = '';
                if (data['status'] == 0) {
                    status = 'не выполнен';
                    status_class = 'light';
                } else {
                    status = 'выполнен';
                    status_class = 'success';
                }
                result += '<div><spam>Имени Пользователя : ' + data['user_name'] + '</spam></div>' +
                    '<div><spam>E-mail : ' + data['user_email'] + '</spam></div>' +
                        '<div><img src="'+(data['images'] != ''? '/'+data['images'] :'/public/img/default.jpg')+'" class="rounded" ></div>' +
                    '<div><spam>Статус : </spam> <span class="badge badge-' + status_class + '">' + status + '</span> </div>' +
                    '<form action="/admin/update_test/' + data['id'] + '" class="update-task" method="post">' +
                    '<div class="form-group">' +
                    '    <label for="exampleText">Текст Задачи</label>' +
                    '    <textarea class="form-control" id="exampleText" rows="3" name="text">' + data['text'] + '</textarea>' +
                    '  </div>' +
                    '<div class="form-check">' +
                    '    <label class="form-check-label">' +
                    '      <input type="checkbox" name="status" class="form-check-input" ' + (data['status'] == 1 ? 'checked' : '') + '>' +
                    '      Выполнен ' +
                    '    </label>' +
                    '  </div>' +
                    '<button type="submit" class="btn btn-primary">Сохранить изменения</button>' +
                    '</form>';
            }
            content.html(result);
        }
    })
});

$(document).on('submit', '.update-task', function (form) {
    form.preventDefault();
    var url = $(this).attr('action');
    var method = $(this).attr('method');
    $.ajax({
        url: url,
        type: method,
        data: $(this).serialize(),
        success: function (data) {
            if (data == 1) {
                window.location.reload()
            }else{
                $('.update-error').remove();
              $('.update-task').append('<div class="alert alert-warning alert-dismissible fade show update-error" role="alert">\n' +
                  ' произошла ошибка , повторите попытку после оерезагрузки страницы .\n' +
                  '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                  '    <span aria-hidden="true">&times;</span>\n' +
                  '  </button>\n' +
                  '</div>')
            }
        }
    })
});
$('#file04').change(function (e) {

    filePath = this.files[0];
    fileName = e.target.files[0];
    var arr = ['image/jpeg','image/png','image/gif'];
    $('label[for="file04"]').text('Выберите картинку');
    if(jQuery.inArray(fileName.type, arr) === -1){
        $('#file04').val('');
        $('.err').show();
        return false;
    }else{
        $('.err').hide();
    }

    $('label[for="file04"]').text(e.target.files[0].name);

    fr = new FileReader();
    fr.onload = function (event) {
        imgPath = event.target.result;
        var el = document.getElementById('vanilla-demo');
        var vanilla = new Croppie(el, {
            viewport: { width: 320, height: 240 },
            boundary: { width: 330, height: 330 },
            showZoomer: false
        });

        vanilla.bind(imgPath).then(function() {
            vanilla.result('canvas','original').then(function (src) {
                $('.see').attr('src', src);
                $('input[name="image"]').val(src)
                vanilla.destroy()
            });
        });
    };
    fr.readAsDataURL(filePath);
});
$('.vanilla-result').click(function () {

    username = $('#username').val();
    email = $('#email').val();
    message = $('#message').val();

    $('.userEmail').text(email);
    $('.userMessage').text(message);
    $('.userName').text(username);

});
$('.userForm').submit(function () {

});





