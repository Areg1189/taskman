<?php
class Home extends System_controller {
    public function index(){

        if(isset($_GET['page'])){
            $page = preg_replace('#[^0-9]#', '', $_GET['page']);
        }else{
            $page = 1;
        }
        $sort = 'id';
        $valid_sort = ['user_name', 'user_email', 'status'];
        if (isset($_GET['sort']) && !empty($_GET['sort']) && in_array($_GET['sort'], $valid_sort)){
            $sort = $_GET['sort'];
        }
        $model = new Models_Taskman();
        $results = $model->get_data($page, 3, $sort);
        $this->view->results = $results;
        $this->view->page = $page;
        $this->view->sort = $sort;
        $this->view->render('home');
    }
}