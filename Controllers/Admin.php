<?php

class Admin extends System_controller
{


    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['auth']['id']) || $_SESSION['auth']['admin'] != true) {
            $this->redirect('/');
        }
    }

    public function index()
    {
        if (isset($_GET['page'])) {
            $page = preg_replace('#[^0-9]#', '', $_GET['page']);
        } else {
            $page = 1;
        }
        $sort = 'id';
        $valid_sort = ['user_name', 'user_email', 'status'];
        if (isset($_GET['sort']) && !empty($_GET['sort']) && in_array($_GET['sort'], $valid_sort)) {
            $sort = $_GET['sort'];
        }
        $model = new Models_Taskman();
        $results = $model->get_data($page, 3, $sort);
        $this->view->results = $results;
        $this->view->page = $page;
        $this->view->sort = $sort;
        $this->view->render('admin/index');
    }

    public function edit($id)
    {

        $id = preg_replace('#[^0-9]#', '', $id);

        if ($id) {
            $model = new Models_Taskman();
            $result = $model->get_single_task($id);
            echo json_encode($result);
            die;
        } else {
            echo 0;
            die;
        }
    }

    public function update_test($id)
    {
        $id = preg_replace('#[^0-9]#', '', $id);
        $id = preg_replace('#[^0-9]#', '', $id);

        $text = $_POST['text'] ?? '';
        $status = isset($_POST['status']) ? 1 : 0;
        if ($id) {
            $model = new Models_Taskman();

            $result = $model->update($id,$text,$status);
            if ($result){
                $_SESSION['message']['alert'] = 'success';
                $_SESSION['message']['text'] = 'Тест обновлен';
                echo 1;die;
            }else{
                echo 0;die;
            }
        } else {
            echo 0;
            die;
        }
    }

}