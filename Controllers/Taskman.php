<?php

class Taskman extends System_controller
{

    public function create()
    {
        $this->view->render('create_task');
    }

    public function store()
    {
        if (isset($_POST['create'])) {

            $user_name = $_POST['user_name'] ?? '';
            $user_email = $_POST['user_email'] ?? '';
            $text = $_POST['text'] ?? '';
            $image = '';
            if ($_POST['image']){
                $data = $_POST['image'];
                if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
                    $data = substr($data, strpos($data, ',') + 1);
                    $type = strtolower($type[1]); // jpg, png, gif

                    if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                        $_SESSION['message']['alert'] = 'warning';
                        $_SESSION['message']['text'] = 'Недопустимый тип изображения';
                        $this->redirect('/taskman/create');
                    }

                    $data = base64_decode($data);

                    if ($data === false) {
                        $_SESSION['message']['alert'] = 'warning';
                        $_SESSION['message']['text'] = 'Задача не создана повторите попытку ';
                        $this->redirect('/taskman/create');;
                    }
                } else {
                    $_SESSION['message']['alert'] = 'warning';
                    $_SESSION['message']['text'] = 'не соответствует URI данных с данными изображения ';
                    $this->redirect('/taskman/create');
                }
                    $image = 'public/img/'.time()."img.{$type}";
                file_put_contents($image, $data);
            }
            $model = new Models_Taskman();
            $result = $model->create($user_name, $user_email, $text, $image);
            if ($result){
                $_SESSION['message']['alert'] = 'success';
                $_SESSION['message']['text'] = 'Сдача успешно создана ';
                $this->redirect('/');
            }else{
                $_SESSION['message']['alert'] = 'warning';
                $_SESSION['message']['text'] = 'Задача не создана повторите попытку ';
                $this->redirect('/taskman/create');
            }
        }else{
            $this->redirect('/');
        }
    }
}