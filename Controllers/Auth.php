<?php
class Auth extends System_controller {
    public function login(){
        if (isset($_SESSION['auth'])){
            $this->redirect('/');
        }else{
            if (isset($_POST['loginForm'])){
                $_SESSION['message']=[];
                if (!isset($_POST['login']) || !isset($_POST['pass']) || empty($_POST['login']) || empty($_POST['pass'])){
                    $_SESSION['message']['alert'] = 'warning';
                    $_SESSION['message']['text'] = 'Неверный логин или пароль';
                    $this->redirect('/auth/login');
                }else{
                    $login = $_POST['login'];
                    $pass = sha1(md5($_POST['pass']));
                    $model = new Models_User();
                    $result = $model->login($login,$pass);
                    if (empty($result)){
                        $_SESSION['message']['alert'] = 'warning';
                        $_SESSION['message']['text'] = 'Неверный логин или пароль';
                        $this->redirect('/auth/login');
                    }else{
                        $_SESSION['auth']['id'] = $result['id'];
                        if ($result['role'] == 1){
                            $_SESSION['auth']['admin'] = true;
                            $redirect = '/admin';
                        }else{
                            $_SESSION['auth']['admin'] = false;
                            $redirect = '/';
                        }
                        $this->redirect($redirect);
                    }

                }
            }else{
                $this->view->render('auth/login');
            }
        }
    }

    public function logout(){
        unset($_SESSION['auth']);
        $this->redirect('/');
    }
}