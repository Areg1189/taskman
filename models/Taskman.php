<?php

class Models_Taskman extends System_model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'taskmans';
    }

    public function get_data($page = 1, $limit = 3, $sort = 'id')
    {
        $result = [];
        $query = $this->db->con->query("select count(*) from $this->table");
        $row = mysqli_fetch_row($query);
        $rows = $row[0];
        $last = ceil($rows / $limit);
        if ($last < 1) {
            $last = 1;
        }
        if ($page < 1) {
            $page = 1;
        } else if ($page > $last) {
            $page = $last;
        }
        $limit = 'LIMIT ' . ($page - 1) * $limit . ',' . $limit;
        $nquery = $this->db->con->query("select * from $this->table ORDER BY $sort ASC  $limit");
        $paginationCtrls = '';
        if ($last != 1) {

            if ($page > 1) {
                $previous = $page - 1;
                $paginationCtrls .= '<li class="page-item ">
                                            <a class="page-link" href="?page=' . $previous . ($sort != 'id' ? '&sort=' . $sort : '') . '" tabindex="-1">Previous</a>
                                        </li> ';

                for ($i = $page - $last; $i < $page; $i++) {
                    if ($i > 0) {
                        $paginationCtrls .= '<li class="page-item"><a class="page-link" href="?page=' . $i . ($sort != 'id' ? '&sort=' . $sort : '') . '">' . $i . '</a></li>';
                    }
                }
            }
            $paginationCtrls .= '<li class="page-item active">
      <a class="page-link" href="?page=' . $page . ($sort != 'id' ? '&sort=' . $sort : '') . '">' . $page . ' <span class="sr-only">(current)</span></a>
    </li>';
            for ($i = $page + 1; $i <= $last; $i++) {
                $paginationCtrls .= '<li class="page-item"><a class="page-link" href="?page=' . $i . ($sort != 'id' ? '&sort=' . $sort : '') . '">' . $i . '</a></li>';
                if ($i >= $page + $last) {
                    break;
                }
            }
            if ($page != $last) {
                $next = $page + 1;
                $paginationCtrls .= '<li class="page-item">
                                      <a class="page-link" href="?page=' . $next . '">Next</a>
                                    </li>';
            }
        }
        $result['paginationCtrls'] = $paginationCtrls;
        $result['rows'] = [];
        while ($crow = mysqli_fetch_array($nquery)) {
            $result['rows'][] = $crow;
        }
        return $result;
    }

    public function get_single_task($id)
    {
        $result = $this->db->select("select * from $this->table where id = '$id'", 1);
        return $result;
    }

    public function update($id, $text, $status)
    {
        $text = $this->db->escape($text);
        $status = $this->db->escape($status);
        $res = $this->db->update($this->table, ['text' => $text, 'status' => $status], "id =".$id);
        return $res;
    }

    public function create($user_name, $user_email, $text, $image ){
        $user_name = $this->db->escape($user_name);
        $user_email = $this->db->escape($user_email);
        $text = $this->db->escape($text);
        $reg = $this->db->insert($this->table,[
            'user_name'=>$user_name, 'user_email'=>$user_email,
            'text'=>$text,  'images'=>$image
        ]);
        return $reg;
    }

}